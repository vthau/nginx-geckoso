FROM nginx:stable-alpine

ADD nginx.conf /etc/nginx/nginx.conf
ADD vhost.conf /etc/nginx/conf.d/default.conf
